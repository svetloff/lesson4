//
//  ViewController.swift
//  Lesson4
//
//  Created by Светлов Андрей on 25.08.2018.
//  Copyright © 2018 svetloff. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        doAllTasks()
    }
    
    func doAllTasks() {
        //doAssignment3Task1()
        //doAssignment3Task2()
        //doAssignment3Task3()
        //doAssignment3Task4()
        //doAssignment3Task5()
        //doAssignment3Task6()
        //doAssignment3Task7()
        //doAssignment3Task8()
        //doAssignment3Task9()
        doAssignment3Task10()
    }
    
    func doAssignment3Task1() {
        let myName = "Svetlov Andrey"
        
        print("Task #1")
        print("\t\t Count of characters in my name:", myName.count)
    }
    
    func doAssignment3Task2() {
        let myPatronymic = "Андреевич"
        
        print("Task #2")
        if myPatronymic.hasSuffix("ич") {
            print("\t\t Suffix in my patronymic: \"ич\"")
        }
        else if myPatronymic.hasSuffix("на") {
            print("\t\t Suffix in my patronymic: \"на\"")
        }
    }
    
    func doAssignment3Task3 () {
        let myFullName = "SvetlovAndreyAndreevich" as NSString
        var myName = ""
        var char = ""
        var arrayOfSubstrings = [String]()
        
        for i in 0..<myFullName.length {
            char = myFullName.substring(with: NSRange.init(location: i, length: 1))
            
            if i == 0 || isUppercaseCharacter(char) {
                arrayOfSubstrings.append(char)
            }
            else {
                arrayOfSubstrings[arrayOfSubstrings.count - 1] += char
            }
        }
        for i in 0..<arrayOfSubstrings.count {
            if myName == "" {
                myName = arrayOfSubstrings[i]
            }
            else {
                myName += " " + arrayOfSubstrings[i]
            }
        }
        
        print("Task #3")
        print("\t\t My full name:", myName)
    }
    
    func isUppercaseCharacter(_ character: String) -> Bool {
        let arrayOfCharacters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
        var result = false
        
        for i in 0..<arrayOfCharacters.count {
            if character == arrayOfCharacters[i] {
                result = true
            }
        }
        
        return result
    }
    
    func doAssignment3Task4() {
        let string = "Andrey" as NSString
        var reverseString = ""
        
        for i in 0..<string.length {
            reverseString += string.substring(with: NSRange.init(location: (string.length - 1) - i, length: 1))
        }
        print("Task #4 string:", string)
        print("\t\t reverse string:", reverseString)
    }
    
    func doAssignment3Task5() {
        let number = "12345678" as NSString
        var numberOnCalc = ""
        var indexTriade = 0
        
        for i in 0..<number.length {
            if indexTriade == 3 {
                numberOnCalc = "," + numberOnCalc
                indexTriade = 0
            }
            numberOnCalc = number.substring(with: NSRange.init(location: (number.length - 1) - i, length: 1)) + numberOnCalc
            indexTriade += 1
        }
        print("Task #5")
        print("\t\t \(numberOnCalc)")
    }
    
    func doAssignment3Task6() {
        let password = "Pa$$w0rd" as NSString
        var char = ""
        var containsDigitChar = 0
        var containsUpperCaseChar = 0
        var containsLowerCaseChar = 0
        var containsSpecialChar = 0
        var containsAllTypesChar = 0
        
        for i in 0..<password.length {
            char = password.substring(with: NSRange.init(location: i, length: 1))
            if isDigitCharacter(char) {
                containsDigitChar = 1
            }
            else if isUppercaseCharacter(char) {
                containsUpperCaseChar = 1
            }
            else if isLowercaseCharacter(char) {
                containsLowerCaseChar = 1
            }
            else {
                containsSpecialChar = 1
            }
        }
        
        if (containsDigitChar + containsUpperCaseChar + containsLowerCaseChar + containsSpecialChar) == 4 {
            containsAllTypesChar = 1
        }
        
        print("Task #6")
        print("\t\t Security of password: \(containsDigitChar + containsUpperCaseChar + containsLowerCaseChar + containsSpecialChar + containsAllTypesChar)")
    }
    
    func isLowercaseCharacter(_ character: String) -> Bool {
        let arrayOfCharacters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
        
        var result = false
        
        for i in 0..<arrayOfCharacters.count {
            if character == arrayOfCharacters[i] {
                result = true
                break
            }
        }
        return result
    }
    
    func isDigitCharacter(_ character: String) -> Bool {
        let arrayOfCharacters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        
        var result = false
        
        for i in 0..<arrayOfCharacters.count {
            if character == arrayOfCharacters[i] {
                result = true
                break
            }
        }
        return result
    }
    
    func doAssignment3Task7() {
        var arrayOfNumbers = [Int]()
        arrayOfNumbers = [9, 9, 7, 3, 8, 6, 9, 1, 5, 3, 7, 7, 3, 3, 0]
        var arrayElement: Int
        var index = 0
        
//        for _ in 0..<15 {
//            arrayOfNumbers.append(Int(arc4random()%10))
//        }
        print("Task #6 array:", arrayOfNumbers)
        for i in 0..<arrayOfNumbers.count {
            for j in 0..<arrayOfNumbers.count {
                if arrayOfNumbers[j] > arrayOfNumbers[i] {
                    arrayElement = arrayOfNumbers[i]
                    arrayOfNumbers[i] = arrayOfNumbers[j]
                    arrayOfNumbers[j] = arrayElement
                }
            }
        }
        print("\t\t sort array: \(arrayOfNumbers)")
        
        while index < (arrayOfNumbers.count - 1) {
            while  ((arrayOfNumbers.count - 1) > index) && (arrayOfNumbers[index + 1] == arrayOfNumbers[index]) {
                arrayOfNumbers.remove(at: index + 1)
            }
            index += 1
        }
        print("\t\t clean array: \(arrayOfNumbers)")
        
    }
    
    func doAssignment3Task8() {
        let transliterationDictionary = createTransliterationDictionary()
        
        let anyWord = "АнДреЙ" as NSString
        var anyWordTransliteration = ""
        var char: String
        
        for i in 0..<anyWord.length {
            char = anyWord.substring(with: NSRange.init(location: i, length: 1))
            
            if char == char.uppercased() {
               anyWordTransliteration += (transliterationDictionary[char.uppercased()] ?? char).uppercased()
            }
            else {
                anyWordTransliteration += (transliterationDictionary[char.uppercased()] ?? char).lowercased()
            }
        }
        print("Task #8 string:", anyWord)
        print("\t\t transliteration:", anyWordTransliteration)
    }
    
    func createTransliterationDictionary() -> [String: String] {
        let result = [
            "А": "A",
            "Б": "B",
            "В": "V",
            "Г": "G",
            "Д": "D",
            "Й": "Y",
            "М": "M",
            "Н": "N",
            "О": "O",
            "Р": "R"]
        
        return result
    }

    func doAssignment3Task9() {
        var anyArray = createArrayTask9()
        var resultArray = [NSString]()
        let searchSubstring = "da"
        
        for i in 0..<anyArray.count {
            if anyArray[i].contains(searchSubstring) {
                resultArray.append(anyArray[i])
            }
        }
        print("Task #9")
        print("\t\t result:", resultArray)
        
    }
    
    func createArrayTask9 () -> [NSString] {
        var result = [NSString]()
        
        result.append("lada")
        result.append("sedan")
        result.append("baklazhan")
        result.append("kukan")
        result.append("dasha")
        
        return result
    }

    func doAssignment3Task10() {
        let setWrongWords = createSetWrongWords()
        let checkPhrase = "hello my fak" as NSString
        var resultPhrase = ""
        
        var arraySubstrings = checkPhrase.components(separatedBy: " ")
        
        for i in 0..<arraySubstrings.count {
            if setWrongWords.contains(arraySubstrings[i].lowercased()) {
                let wrongWord = arraySubstrings[i] as NSString
                arraySubstrings[i] = ""
                for _ in 0..<wrongWord.length {
                    arraySubstrings[i] += "*"
                }
            }
            if resultPhrase == "" {
                resultPhrase += arraySubstrings[i]
            }
            else {
                resultPhrase += " " + arraySubstrings[i]
            }
        }
        print("Task #10")
        print("\t\t result:", resultPhrase)
    }
    
    func createSetWrongWords() -> Set<String> {
        let result: Set = ["fuck", "fak"]
        
        return result
    }
    
}

